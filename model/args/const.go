package args

const (
	UserInfoSearchNoticeType        = 50001
	MerchantsMaterialInfoNoticeType = 50002
)

const (
	eSIndexPrefix                = "micro-mall-"
	ESIndexUserInfo              = eSIndexPrefix + "user-info"
	ESIndexMerchantsMaterialInfo = eSIndexPrefix + "merchants-material-info"
)

type MerchantInfoSearch struct {
	Uid          int64  `json:"uid"`
	UserName     string `json:"user_name"`
	MerchantCode string `json:"merchant_code"`
	RegisterAddr string `json:"register_addr"`
	HealthCardNo string `json:"health_card_no"`
	TaxCardNo    string `json:"tax_card_no"`
}

type UserInfoSearch struct {
	UserName    string `json:"user_name"`
	Phone       string `json:"phone"`
	Email       string `json:"email"`
	IdCardNo    string `json:"id_card_no"`
	ContactAddr string `json:"contact_addr"`
}

type CommonBusinessMsg struct {
	Type    int    `json:"type"`
	Tag     string `json:"tag"`
	UUID    string `json:"uuid"`
	Content string `json:"content"`
}
