package vars

import (
	es7 "github.com/elastic/go-elasticsearch/v7"
)

var (
	EmailConfigSetting   *EmailConfigSettingS
	ElasticsearchSetting *ElasticsearchSettingS
	ElasticsearchClient  *es7.Client
)
