package vars

type EmailConfigSettingS struct {
	User     string
	Password string
	Host     string
	Port     string
}

type ElasticsearchSettingS struct {
	Addresses string `json:"addresses"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}
