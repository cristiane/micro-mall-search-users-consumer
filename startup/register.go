package startup

import "gitee.com/cristiane/micro-mall-search-users-consumer/service"

const (
	UserInfoSearchNotice    = "user_info_search_notice"
	UserInfoSearchNoticeErr = "user_info_search_notice_err"
)

func GetNamedTaskFuncs() map[string]interface{} {

	var taskRegister = map[string]interface{}{
		UserInfoSearchNotice:    service.NoticeConsume,
		UserInfoSearchNoticeErr: service.NoticeConsumeErr,
	}
	return taskRegister
}
