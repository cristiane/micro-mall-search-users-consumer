package code

import "gitee.com/kelvins-io/common/errcode"

const (
	ErrESSearch = 29999999
)

var ErrMap = make(map[int]string)

func init() {
	dict := map[int]string{
		ErrESSearch: "搜索错误",
	}
	errcode.RegisterErrMsgDict(dict)
	for key, _ := range dict {
		ErrMap[key] = dict[key]
	}
}
